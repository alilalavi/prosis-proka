<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'caption' => "شروع نشده",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('statuses')->insert([
            'caption' => "شروع شده",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('statuses')->insert([
            'caption' => "درحال انجام",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('statuses')->insert([
            'caption' => "انجام شده",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('statuses')->insert([
            'caption' => "رد شده",
          'created_at' => now(),
          'updated_at' => now(),
        ]);

    }
}
