<?php

use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priorities')->insert([
          'caption' => "مهم و فوری",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('priorities')->insert([
          'caption' => "مهم و غیر فوری",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('priorities')->insert([
          'caption' => "غیر مهم و فوری",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('priorities')->insert([
          'caption' => "غیر مهم غیر فوری",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
        DB::table('priorities')->insert([
          'caption' => "فوق العاده",
          'created_at' => now(),
          'updated_at' => now(),
        ]);
    }
}
