<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            /*-----------------------------------------------*/
            $table->unsignedBigInteger('project_id')->nullable()->default(null);
            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            /*-----------------------------------------------*/
            $table->unsignedBigInteger('status_id')->nullable()->default(null);
            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            /*------------------------------------------------*/
            $table->unsignedBigInteger('priority_id')->nullable()->default(null);
            $table->foreign('priority_id')
                ->references('id')
                ->on('priorities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            /*------------------------------------------------*/
		        $table->date('start_time')->nullable()->default(null);
		        $table->date('deadline')->nullable()->default(null);
		        $table->date('reject')->nullable()->default(null);
		        $table->date('done')->nullable()->default(null);
            $table->timestamps();
        });

        /*New Pivot Table*/
        Schema::create('task_user', static function (Blueprint $table) {
            $table->unsignedBigInteger('task_id');
            $table->foreign('task_id')
                ->references('id')
                ->on('tasks');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
