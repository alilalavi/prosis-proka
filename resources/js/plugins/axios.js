import Vue from 'vue';
import axios from "axios";
import {auth} from '../store/auth.module';

axios.interceptors.request.use(request => {
	const token = auth.mutations['auth/token'];
	if (token) {
		request.headers.common['Authorization'] = `Bearer ${token}`
	}
	return request
});