import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'api/auth/';

class UserService {
	getUserBoard() {
		return axios.get(API_URL + 'user/get', { headers: authHeader() });
	}
	getPerson() {
		return axios.get(API_URL + 'person/get', { headers: authHeader() });
	}
	getPriority() {
		return axios.get(API_URL + 'priority/get', { headers: authHeader() });
	}
	getStatus() {
		return axios.get(API_URL + 'status/get', { headers: authHeader() });
	}
	getProjects() {
		return axios.get(API_URL + 'projects/get', { headers: authHeader()});
	}
	getTasks() {
		return axios.get(API_URL + 'tasks/get', { headers: authHeader() });
	}
	getPeople() {
		return axios.get(API_URL + 'people/get', { headers: authHeader() });
	}
}

export default new UserService();
