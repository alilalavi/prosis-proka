import axios from "axios";
import authHeader from './auth-header';


const API_URL = 'api/auth/';

export default function newProject(Project) {
		return axios
			.post(API_URL + 'newProject', {
				name: Project.name,
				description: Project.description,
			} ,{ headers: authHeader() })
			.then(response => {
				return response.data;
			});
	}
