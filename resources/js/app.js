import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';
import './plugins/vuetify';
import './plugins/Vuesax';
import './plugins/VueMaterial';
// import 'vue-material-design-icons/styles.css';
import App from './App.vue';
import {router} from './router/index.js';
import store from './store/index.js';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
    faHome,
    faUser,
    faUserPlus,
    faSignInAlt,
    faSignOutAlt
} from '@fortawesome/free-solid-svg-icons';
import VeeValidate from 'vee-validate';
import VueMaterial from "vue-material";
import Vuesax from 'vuesax';
import Axios from "axios";
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token');
if (token) {
	Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

Vue.use(Vuesax);
Vue.use(VueMaterial);
Vue.use(VeeValidate);
library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt);
Vue.config.productionTip = false;
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(Vuetify);
Vue.use(VuePersianDatetimePicker, {
    name: 'pdatepicker',
    props: {
        inputFormat: 'YYYY-MM-DD',
        format: 'jYYYY-jMM-jDD',
        editable: false,
        inputClass: 'form-control md-4',
        placeholder: 'لطفا تاریخ تولد خود را وارد کنید',
        altFormat: 'YYYY-MM-DD',
        color: '#00acc1',
        autoSubmit: false,
        }
});

window.eventHub = new Vue();
Vue.component('App', require('./App.vue').default);

new Vue({
    rtl:true,
    Vuesax,
    VueMaterial,
    router,
    store,
    render: h => h(App)
}).$mount('#app');


