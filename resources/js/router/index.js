import Vue from 'vue';
import Router from 'vue-router';
import Auth from '../views/Auth.vue';

Vue.use(Router);

export const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Auth',
			component: Auth
		},
		{
			path: '/Auth',
			name: 'auth',
			component: Auth
		},
		{
			path: '/dashboard',
			name: 'Dashboard',
			// lazy-loaded
			component: () => import('../views/Dashboard.vue')
		},
		{
			path: '/projects',
			name: 'Projects',
			// lazy-loaded
			component: () => import('../views/Projects.vue')
		},
		{
			path: '/task',
			name: 'Task',
			// lazy-loaded
			component: () => import('../views/Tasks.vue')
		}
	]
});

router.beforeEach((to, from, next) => {
	const publicPages = ['/', '/Auth'];
	const authRequired = !publicPages.includes(to.path);
	const loggedIn = localStorage.getItem('user');

	// try to access a restricted page + not logged in
	if (authRequired && !loggedIn) {
		return next('/Auth');
	}

	next();
});
