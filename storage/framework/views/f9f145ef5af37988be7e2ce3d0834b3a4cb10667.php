<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
	<title><?php echo e(config('app.name', 'Proka ')); ?></title>
	<!-- Scripts -->
	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<!-- Styles -->
	<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>
<body>
	<div id="app">
	</div>
	<script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
	<script src="<?php echo e(asset('0.js')); ?>" defer></script>
	<script src="<?php echo e(asset('1.js')); ?>" defer></script>
	<script src="<?php echo e(asset('2.js')); ?>" defer></script>
	<script src="<?php echo e(asset('3.js')); ?>" defer></script>
</body>
</html>
<?php /**PATH /home/vagrant/test5/resources/views/import.blade.php ENDPATH**/ ?>