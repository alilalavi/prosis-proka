<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'APIController@login');
    Route::post('register', 'APIController@register');
    Route::group(['middleware' => 'auth.jwt'], static function () {
        Route::get('/user/get', static function (Request $request) {
            return $request->user();
        });
        Route::post('logout', 'APIController@logout');
        Route::post('assignTaskUser/store', 'TaskController@assignTaskUser');
        Route::post('newtask/create', 'TaskController@createTask');
        Route::post('newproject/create', 'ProjectController@createProject');
        Route::get('priority/get', 'ProjectController@getPriority');
        Route::get('status/get', 'ProjectController@getStatusAll');
        Route::get('projects/get', 'ProjectController@getProjects');
        Route::get('tasks/get', 'ProjectController@getTasks');
        Route::get('people/get', 'ProjectController@getPeople');
    });
});
