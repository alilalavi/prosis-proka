<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//      $this->app->bind('path.public', static function() {
//        return realpath(base_path().'/../public_html');
//      });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//      $this->app->bind('path.public', static function() {
//        return dirname(base_path()) . '/public_html';
//      });
    }
}
