<?php

namespace App\_MainPart\DataAccessLayer;

use App\Model\Task;
use App\Model\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class TaskDAL
{

    public function createNewTask($task)
    {
        if (JWTAuth::user()) {
            $tasks = new Task();
            $tasks->title = $task->title;
            $tasks->description = $task->description;
            $tasks->project_id = $task->project_id;
            $tasks->status_id = $task->status_id;
            $tasks->priority_id = $task->priority_id;
            $tasks->start_time = $task->start_time;
            $tasks->deadline = $task->deadline;
            $tasks->reject = $task->reject;
            $tasks->done = $task->done;
            $tasks->save();
            if ($tasks) {
                $tasks->users()->attach(JWTAuth::user());
            }
        }
    }

    public function assignTaskUser($taskId, $userId)
    {
        $user = User::find((int) $userId);
        $task = Task::find((int) $taskId);
        $task->users()->sync($userId);
        // $task->users()->sync($userId);
        return response()->json([
            'success' => true,
            'task' => $task,
        ]);

    }
}
