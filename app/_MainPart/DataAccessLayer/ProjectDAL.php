<?php

namespace App\_MainPart\DataAccessLayer;

use App\Http\Resources\Project as ProjectResource;
use App\Model\Person;
use App\Model\Project;
use App\Model\Status;
use App\Model\Priority;

class ProjectDAL
{
      protected $user;

    public function __construct()
    {
      $this->user = \JWTAuth::parseToken()->authenticate();
    }
    public function createProject($projects)
    {
        $project = new Project();
        $project->name = $projects->name;
        $project->user_id = $this->user->getJWTIdentifier();
        $project->description = $projects->description;
        $project->save();
    }
    public function getStatus()
    {
      return Status::all();
    }
    public function getPriority()
    {
      return Priority::all();
    }
    public function getPerson()
    {
      return $this->user->person()->get();
    }
    public function getProjects()
    {
      return ProjectResource::collection($this->user->project()->get());
    }
    public function getTasks()
    {
  //      return TaskResource::collection($this->user->tasks()->get()) ;
      return $this->user->tasks()->get();
    }
    public function getPeople()
    {
      return Person::all();
    }
}
