<?php


namespace App\_MainPart\BusinessLayer;

use App\_MainPart\DataAccessLayer\TaskDAL;


class TaskBL
{
    public function createNewTask($task)
    {
      return (new TaskDAL())->createNewTask($task);
    }

    public function assignTaskUser($input)
    {
      foreach ($input as $key => $value)
              {
                if ($key === 'taskId'){
                  $taskId = $value;
                }
                else{
                  $userId = $value;
                }
              }
      return (new TaskDAL())->assignTaskUser($taskId , $userId);

    }
}
