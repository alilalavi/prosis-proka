<?php

namespace App\_MainPart\BusinessLayer;

use App\_MainPart\DataAccessLayer\ProjectDAL;
/**
 * @method validate($project, array $array)
 */
class ProjectBL
{
    public function createNewProject($project)
    {
        return (new ProjectDAL())->createProject($project);
    }
    public function getStatuses()
    {
        return (new ProjectDAL)->getStatus();
    }
    public function getPriority()
    {
        return (new ProjectDAL)->getPriority();
    }
    public function getPerson()
    {
        return (new ProjectDAL)->getPerson();
    }
    public function getProjects()
    {
      return (new ProjectDAL)->getProjects();
    }
    public function getTasks()
    {
      return (new ProjectDAL)->getTasks();
    }
    public function getPeople()
    {
      return (new ProjectDAL)->getPeople();
    }
}
