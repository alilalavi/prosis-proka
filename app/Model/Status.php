<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
        'caption'
    ];

    public function task()
    {
        return $this->hasMany(Task::class);
    }
}
