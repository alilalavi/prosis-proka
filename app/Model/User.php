<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property mixed name
 * @property mixed email
 * @property string password
 * @property mixed id
 * @method static create(array $all)
 * @method static find(int $int)
 * @method static get()
 */
class User extends Authenticatable implements JWTSubject

{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function project()
    {
        return $this->hasMany(Project::class);
    }
    public function person()
    {
        return $this->hasMany(Person::class);
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function tasks()
    {
        return $this->belongsToMany(
          Task::class,
          'task_user' ,
          'user_id',
          'task_id' )
          ->using(TaskUser::class);
    }
}
