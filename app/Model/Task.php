<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed title
 * @property mixed description
 * @property mixed project_id
 * @property mixed status_id
 * @property mixed priority_id
 * @property mixed start_time
 * @property mixed deadline
 * @property mixed reject
 * @property mixed done
 *  @method static find(int $int)
 * @method static create($task)
 */
class Task extends Model
{
        protected $casts = [
        'project_id' => 'int',
        'status_id' => 'int',
        'priority_id' => 'int'
    ];

//    protected $dates = [
//        'start_time',
//        'deadline',
//        'reject',
//        'done'
//    ];

    protected $fillable = [
        'title',
        'description',
        'project_id',
        'status_id',
        'priority_id',
        'start_time',
        'deadline',
        'reject',
        'done'
    ];

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function project()
    {
        return $this->hasMany(Project::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function users()
    {
        return $this->belongsToMany(
          User::class ,
          'task_user' ,
          'task_id' ,
          'user_id')
          ->using(TaskUser::class);
    }

}
