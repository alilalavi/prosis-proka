<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property mixed name
 * @property mixed description
 * @method static create(array $data)
 */
class Project extends Model
{
    protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'name',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

}

