<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property array|string|null username
 * @property array|string|null email
 */
class Person extends Model
{

	protected $casts = [
        'user_id' => 'int'
    ];

    protected $dates = [
        'birth_date'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'birth_date',
        'phone',
        'photo_url'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
