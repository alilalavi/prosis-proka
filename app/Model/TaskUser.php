<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TaskUser extends Pivot
{
  protected $table = 'task_user';
  public $incrementing = false;
  public $timestamps = false;

  protected $casts = [
    'task_id' => 'int',
    'user_id' => 'int'
  ];

  protected $fillable = [
    'task_id',
    'user_id'
  ];

  public function task()
  {
    return $this->belongsTo(Task::class, 'task_id', 'id');

  }
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');

  }
}
