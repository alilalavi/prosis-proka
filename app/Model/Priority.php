<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $fillable = [
        'caption'
    ];

    public function task()
    {
        return $this->hasMany(Task::class);
    }
}
