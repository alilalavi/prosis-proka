<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class ProjectFormRequest extends FormRequest
{
    public function rules()
    {
        return [
          'name' => 'required|string|min:3|max:50',
          'description' => 'required|string|min:3|max:150'
        ];
    }
}
