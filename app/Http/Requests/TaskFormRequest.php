<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskFormRequest extends FormRequest
{
    public function rules()
    {
      return [
        'title' => 'required|string|min:3|max:50',
        'description' => 'required|string|min:6|max:150',
        'project_id' => 'required|int|min:1',
        'status_id' => 'required|int|min:1',
        'priority_id' => 'required|int|min:1',
        'start_time' => '',
        'deadline' => '',
        'reject' => '',
        'done' => '',
      ];
    }
}
