<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectFormRequest;
use App\Http\Resources\Project as ProjectResource;
use App\Model\Project;
use App\_MainPart\BusinessLayer\ProjectBL;
use JWTAuth;

class ProjectController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function createProject(ProjectFormRequest $request)
    {
        if ($validated = $request->validated()) {
            $project = new Project($request->all());
            $projects = new ProjectBL();
            $projects->createNewProject($project);
            return response()->json([
                'success' => true,
                'project' => $project,
                'user' => ProjectResource::collection($this->user->project()->get()),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be added.',
            ], 500);
        }

    }

    public function getStatusAll()
    {
        return (new ProjectBL)->getStatuses();
    }

    public function getPriority()
    {
        return (new ProjectBL)->getPriority();
    }
    public function getPerson()
    {
        return (new ProjectBL)->getPerson();
    }
    public function getProjects()
    {
      return (new ProjectBL)->getProjects();
    }
    public function getTasks()
    {
      return (new ProjectBL)->getTasks();
    }
    public function getPeople()
    {
        return (new ProjectBL)->getPeople();
    }

}
