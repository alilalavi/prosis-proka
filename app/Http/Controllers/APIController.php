<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use App\Model\Person;
use App\Model\User;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Requests\RegistrationFormRequest;

class APIController extends Controller
{

    public $loginAfterSignUp = true;

    public function login(LoginFormRequest $request)
    {
        $validated = $request->validated();
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
        $user = JWTAuth::user();
        return response()->json([
            'success' => true,
            'token' => $token,
            'user' => $user,
        ]);
    }
    public function logout(Request $request)
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
	public function register(RegistrationFormRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
	      $validated = $request->validated();
        $user->save();

        $person = new Person();
        $person->user_id = $user->id;
        $person->username = $request->input('name');
        $person->email = $request->input('email');
        $person->save();

//        if ($this->loginAfterSignUp) {
//            return $this->login($request);
//        }
       return response()->json([
            'success'   =>  true,
            'data'      =>  $user
        ], 200);
    }
}
