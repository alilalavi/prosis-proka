<?php

namespace App\Http\Controllers;

use App\_MainPart\BusinessLayer\TaskBL;
use App\Http\Requests\TaskFormRequest;
use App\Model\Task;
use App\Model\User;
use Illuminate\Http\Request;
use JWTAuth;

class TaskController extends Controller
{
        protected $user;

        public function __construct()
          {
              $this->user = JWTAuth::parseToken()->authenticate();
          }

        public function createTask(TaskFormRequest $request)
          {
            if ($validated = $request->validated()){
              $task = new Task($request->all());
              $tasks = new TaskBL();
              $tasks->createNewTask($task);
              return response()->json([
                'success' => true,
                'task' => $task,
                'user' => $this->user
              ]);
            }
            else
              {
                return response()->json([
                  'success' => false,
                  'message' => 'Sorry, task could not be added.'
                ], 500);
              }

          }
          public function assignTaskUser(Request $request)
              {
                $taskId = '';
                $userId = [];
                $input = $request->input(['input']);
                return (new TaskBL)->assignTaskUser($input);
              }
}
