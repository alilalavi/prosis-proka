<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed id
 * @property mixed phone
 * @property mixed gender
 * @property mixed email
 * @property mixed photo_url
 * @property mixed created_at
 * @property mixed user
 */
class Person extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'user'          => new User($this->user),
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'gender'        => $this->gender,
            'phone'         => $this->phone,
            'email'         => $this->email,
            'photo_url'     => $this->photo_url,
            'birth_date'    => $this->created_at->format('y,m,d,h,m'),
            'created_at'    => $this->created_at->format('y,m,d,h,m'),
            'updated_at'    => $this->updated_at->format('y,m,d,h,m'),
        ];
    }
}
