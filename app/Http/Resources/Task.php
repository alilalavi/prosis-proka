<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed title
 * @property mixed description
 * @property mixed project
 * @property mixed status
 * @property mixed priority
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed pivot
 */
class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id'            => $this->id,
        'title'         => $this->title,
        'description'   => $this->description,
//        'user'          => User::collection($this->whenLoaded('tasks')),
        'project'       => new Project($this->project),
        'status'        => new Status($this->status),
        'priority'      => new Priority($this->priority),
        'start_time'    => $this->created_at->format('y,m,d,h,m'),
        'deadline'      => $this->created_at->format('y,m,d,h,m'),
        'reject'        => $this->created_at->format('y,m,d,h,m'),
        'done'          => $this->created_at->format('y,m,d,h,m'),
        'created_at'    => $this->created_at->format('y,m,d,h,m'),
        'updated_at'    => $this->updated_at->format('y,m,d,h,m'),
      ];
    }
}
