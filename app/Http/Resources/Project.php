<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed user
 * @property mixed name
 * @property mixed description
 */
class Project extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'user'     => new User($this->user),
            'name'       => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at->format('y,m,d,h,m'),
            'updated_at' => $this->updated_at->format('y,m,d,h,m'),
        ];
    }
}
