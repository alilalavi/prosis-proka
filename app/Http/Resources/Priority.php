<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed caption
 * @property mixed created_at
 * @property mixed email
 * @property mixed updated_at
 */

class Priority extends JsonResource
{

    public function toArray($request)
    {
        return [
          'id'         => $this->id,
          'caption'    => $this->caption,
          'created_at' => $this->created_at->format('y,m,d,h,m'),
          'updated_at' => $this->updated_at->format('y,m,d,h,m'),
        ];
    }
}
