<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed task_id
 * @property mixed user_id
 */
class TaskUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'task_id' => $this->task_id,
        'user_id' => $this->user_id,
      ];
    }
}
